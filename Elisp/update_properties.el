(require 'cl)
(defun create-taskjuggler-taskid ()
  ""
  (interactive)
  (let (taskids)
    (org-map-entries
     (lambda ()
       (
	(org-entry-put (point) "TASK_ID" (org-entry-get (point) "TASKID")))
       )))
  )
		 
(defun copy-property-value-to-new-property (original-property-name new-property-name)
  ""
  nil
  (org-map-entries
     (lambda ()
       (
	(org-entry-put (point) original-property-name (org-entry-get (point) new-property-name)))
       )))

(require 'ert)

(require 'noflet)
(ert-deftest copy-property-value-to-new-property-test ()
  ""
  (let ((value "") (foo "foo") (bar "bar"))
    (noflet ((org-entry-get (POM PROPERTY &optional INHERIT LITERAL-NIL) "42")
	     (org-entry-put (POM PROPERTY VALUE) (setq value VALUE)))
      (should (equal (copy-property-value-to-new-property 'foo 'bar) "42")))))


