;; This snippet adds a new property to an element. The new property value comes from an existing
;; property. I use this to convert scrum.el TASKIDs into ox-taskjuggler TASK_IDs.
(org-map-entries (lambda ()
		   (org-entry-put (point) "TASK_ID" (org-entry-get (point) "TASKID"))
		   ))
