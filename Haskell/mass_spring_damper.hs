import Text.Printf

xi = 0.0
xi' = 0.0
dt = 0.01
dampingCoefficient = 8.880
gravity  = 9.880
mass = 1.0
springCoefficient = 39.470
diffeq :: (Double, Double) -> Double -> (Double, Double)
diffeq (xpos, xvel) dt  =
  let x'' =
        -(springCoefficient * xpos + dampingCoefficient * xvel) / mass - gravity
  in let x =
           xpos + xvel * 0.01
     in let x' =
              xvel + x'' * 0.01
        in (x, x')

run =
  scanl diffeq (0.0, 0.0) [0, 0.01..2.5]

toColumns =
  mapM_ (\(a,b) -> printf "%.16f\t%.16f\n" a b ) run

main :: IO()
main = 
  toColumns
